﻿using Chaos.NaCl;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace IcoStarter.Common.Crypto
{
    public class KeyPair
    {
        public byte[] PublicKeyBytes => Ed25519.PublicKeyFromSeed(_privateSeed);

        public string _publicKey;
        public string PublicKey
        {
            get
            {
                return _publicKey ?? (_publicKey = KeyToString(AccountIdByte, PublicKeyBytes));
            }
        }

        private string _secretKey;
        public string SecretKey
        {
            get
            {
                return _secretKey ?? (_secretKey = KeyToString(SecretKeyByte, _privateSeed));
            }
        }

        private byte[] _privateSeed = new byte[32];

        public KeyPair()
        {
            var crypto = RNGCryptoServiceProvider.Create();

            crypto.GetBytes(_privateSeed);
        }

        public KeyPair(byte[] bytes)
        {
            _privateSeed = bytes;
        }

        private const byte AccountIdByte = 6 << 3;
        private const byte SecretKeyByte = 18 << 3;

        private string KeyToString(byte versionByte, byte[] key)
        {
            var bytes = new List<byte> { versionByte };
            bytes.AddRange(key);
            bytes.AddRange(XmodemCRC16.Calculate(bytes.ToArray()));
            return Base32Encoding.Encode(bytes.ToArray());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IcoStarter.Common.Crypto
{
    public static class XmodemCRC16
    {
        public static byte[] Calculate(IEnumerable<byte> bytes)
        {
            // Xmodem CRC16
            uint crc = 0;

            foreach (var value in bytes)
            {
                uint code = (crc >> 8) ^ value;
                code ^= code >> 4;

                crc = crc << 8
                    ^ code
                    ^ (code << 5)
                    ^ (code << 12);

                crc &= 0xFFFF;
            }

            return new[]
            {
                (byte)crc,
                (byte)(crc >> 8)
            };
        }
    }
}

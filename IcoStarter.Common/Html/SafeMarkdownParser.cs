﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IcoStarter.Common.Html
{
    public class AllowedTagWithAttributes
    {
        public string Tag { get; set; }

        public string[] Attributes { get; set; }
    }

    public class SafeMarkdownParser
    {
        private string[] _allowedRawTags = new[]
        {
            "s",
            "b",
            "i",
            "u",
            "table",
            "tr",
            "td",
            "thead",
            "tbody",
            "tfoot",
            "hr"
        };
        
        public string ParseEncodedHtml(string encodedHtml)
        {
            return encodedHtml;
        }
    }
}

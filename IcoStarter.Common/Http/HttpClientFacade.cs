﻿using IcoStarter.Common.HTTP;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace IcoStarter.Common.Http
{
    [Inject(Lifetime.Singleton)]
    public class HttpClientFacade : IHttpClient
    {
        private HttpClient _httpClient;

        public HttpClientFacade()
        {
            _httpClient = new HttpClient();
        }

        public async Task<string> GetAsync(string uri)
        {
            var response = await _httpClient.GetAsync(uri);

            if(!response.IsSuccessStatusCode)
            {
                return null;
            }

            return await response.Content.ReadAsStringAsync();
        }
    }
}

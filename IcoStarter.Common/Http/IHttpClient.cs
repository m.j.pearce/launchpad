﻿using System.Threading.Tasks;

namespace IcoStarter.Common.HTTP
{
    public interface IHttpClient
    {
        Task<string> GetAsync(string uri);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IcoStarter.Common
{
    public enum Lifetime
    {
        Transient,
        PerRequest,
        Singleton
    }

    public class InjectAttribute : Attribute
    {
        public InjectAttribute(Lifetime lifetime = Lifetime.Transient)
        {
            Lifetime = lifetime;
        }

        public Lifetime Lifetime { get; set; }
    }
}

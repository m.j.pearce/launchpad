﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IcoStarter.Common
{
    [Inject(Lifetime.Singleton)]
    public class Settings : ISettings
    {
        public string HorizonUri => "https://horizon-testnet.stellar.org";

        public int HorizonPageSize => 10;

        public int AccountBalanceUpdateFrequencySeconds => 5;

        public int AccountTransactionsUpdateFrequencySeconds => 5;

        public int AccountPaymentsUpdateFrequencySeconds => 5;

        public bool IsDevEnvironment => Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";

        public string DatabaseUrl => Environment.GetEnvironmentVariable("DATABASE_URL");

        public string AwsAccessKeyId => Environment.GetEnvironmentVariable("AWS_ACCESS_KEY_ID") ?? "AKIAIYQG6VC2J6AXTSEA";

        public string AwsSecretAccessKey => Environment.GetEnvironmentVariable("AWS_SECRET_ACCESS_KEY");

        public string S3BucketName => Environment.GetEnvironmentVariable("S3_BUCKET_NAME");
    }
}

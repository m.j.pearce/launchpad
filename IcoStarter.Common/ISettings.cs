﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IcoStarter.Common
{
    public interface ISettings
    {
        string HorizonUri { get; }

        int HorizonPageSize { get; }
        
        int AccountBalanceUpdateFrequencySeconds { get; }

        int AccountTransactionsUpdateFrequencySeconds { get; }

        int AccountPaymentsUpdateFrequencySeconds { get; }

        bool IsDevEnvironment { get; }

        string DatabaseUrl { get; }

        string AwsAccessKeyId { get; }

        string AwsSecretAccessKey { get; }

        string S3BucketName { get; }
    }
}

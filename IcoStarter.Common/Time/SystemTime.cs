﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IcoStarter.Common.Time
{
    // Ensures UTC time is always used, and allows manually setting the time for testing
    public static class SystemTime
    {
        private static DateTime? _fixedTimeForTesting;

        public static bool IsFixedForTesting => _fixedTimeForTesting.HasValue;

        public static void SetFixedTimeForTesting(DateTime? time)
        {
            _fixedTimeForTesting = time;
        }

        public static DateTime Now => IsFixedForTesting ? _fixedTimeForTesting.Value : DateTime.UtcNow;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IcoStarter.Common.Time
{
    public enum Units
    {
        Seconds,
        Days,
        Hours
    }

    public class TimeLeftCalculator
    {
        public Units Units { get; private set; }

        public int Amount { get; private set; }

        public void Calculate(DateTime endTime)
        {
            var timeLeft = endTime - SystemTime.Now;

            if (timeLeft.TotalDays > 1.0)
            {
                Units = Units.Days;
                Amount = (int)timeLeft.TotalDays;
            }
            else if(timeLeft.TotalHours > 1.0)
            {
                Units = Units.Hours;
                Amount = (int)timeLeft.TotalHours;
            }
            else if (timeLeft.TotalSeconds > 0.0)
            {
                Units = Units.Seconds;
                Amount = (int)timeLeft.TotalSeconds;
            }
            else
            {
                Units = Units.Seconds;
                Amount = 0;
            }
        }
    }
}

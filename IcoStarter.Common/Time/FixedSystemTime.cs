﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IcoStarter.Common.Time
{
    // Use within a using(...) block to fix the system time for a period (useful for tests)
    public class FixedSystemTime : IDisposable
    {
        private DateTime? _oldSystemTime;

        public FixedSystemTime(DateTime fixedSystemTime)
        {
            _oldSystemTime = SystemTime.IsFixedForTesting ? SystemTime.Now : (DateTime?)null;

            SystemTime.SetFixedTimeForTesting(fixedSystemTime);
        }

        public void Dispose()
        {
            // Reset to last system time (to support nesting of using blocks)
            SystemTime.SetFixedTimeForTesting(_oldSystemTime);
        }
    }
}

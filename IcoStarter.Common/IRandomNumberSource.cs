﻿using System;
namespace IcoStarter.Common
{
    public interface IRandomNumberSource
    {
        int GetNumber(int min, int max);

        int[] GetNumbers(int min, int max, int count);
    }
}

﻿using System;
using System.Linq;
using System.Security.Cryptography;
namespace IcoStarter.Common
{
    [Inject(Lifetime.Singleton)]
    public class RandomNumberSource : IRandomNumberSource
    {
        private readonly RandomNumberGenerator _randomNumberGenerator;

        public RandomNumberSource() => _randomNumberGenerator = RandomNumberGenerator.Create();

        public int GetNumber(int min, int max)
        {
            return GetNumbers(min, max, 1)[0];
        }

        public int[] GetNumbers(int min, int max, int count)
        {
            var data = new byte[count];

            _randomNumberGenerator.GetBytes(data);

            return data.Select(d => (d % (max - min)) + min).ToArray();
        }
    }
}

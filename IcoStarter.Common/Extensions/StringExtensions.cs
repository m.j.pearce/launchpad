﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IcoStarter.Common.Extensions
{
    public static class StringExtensions
    {
        public static string ToShortName(this string input)
        {
            return string.Concat(input.ToLower()
                .Select(c =>
                {
                    if (c >= 'a' && c <= 'z')
                    {
                        return c.ToString();
                    }
                    else if (c == ' ')
                    {
                        return "-";
                    }
                    else
                    {
                        return "";
                    }
                }));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using IcoStarter.Data;
using IcoStarter.Models;
using IcoStarter.Services;
using IcoStarter.Common;
using System.Reflection;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Rewrite;
using IcoStarter.HorizonClient;

namespace IcoStarter
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        private string PostgresUrlToConnectionString(string url, bool requireSsl)
        {
            if(!(url?.StartsWith("postgres://") ?? false)) 
            {
                return null;
            }
            
            url = url.Replace("postgres://", "");

            var parts = url.Split(new[] {':', '@', '/'});

            var user = parts[0];
            var pass = parts[1];
            var host = parts[2];
            var port = parts[3];
            var dbName = parts[4];

            return $"Server={host};Port={port};Database={dbName};Username={user};Password={pass};SSL Mode={(requireSsl ? "Require" : "Disable")};Trust Server Certificate=true";
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var settings = new Settings();

            services.AddSingleton<ISettings>(settings);

            var connectionString = PostgresUrlToConnectionString(settings.DatabaseUrl, settings.DatabaseUrl.ToLower().Contains("amazonaws.com"));

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseNpgsql(connectionString),
                contextLifetime: ServiceLifetime.Transient, optionsLifetime: ServiceLifetime.Transient);

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 5;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredUniqueChars = 1;
            });

            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = options.LoginPath.ToString().ToLower();
                options.LogoutPath = options.LogoutPath.ToString().ToLower();
                options.ReturnUrlParameter = options.ReturnUrlParameter.ToLower();
            });

            var assemblies = new[]
            {
                GetType().Assembly,
                typeof(ISettings).Assembly,
                typeof(HorizonClient.HorizonClient).Assembly
            };

            services.AddTransient<IBackgroundUnitOfWork, UnitOfWork>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            var injectable = assemblies.SelectMany(t => t.GetTypes())
                .Select(t => new { Type = t, Inject = (InjectAttribute)t.GetCustomAttributes(typeof(InjectAttribute), false).SingleOrDefault() })
                .Where(t => t.Inject != null);

            foreach(var type in injectable)
            {
                var interfaces = type.Type.GetInterfaces();

                foreach(var implementedInterface in interfaces)
                {
                    switch (type.Inject.Lifetime)
                    {
                        case Lifetime.Transient:
                            services.AddTransient(implementedInterface, type.Type);
                            break;

                        case Lifetime.PerRequest:
                            services.AddScoped(implementedInterface, type.Type);
                            break;

                        case Lifetime.Singleton:
                            services.AddSingleton(implementedInterface, type.Type);
                            break;
                    }
                }
            }

            services.Configure<RouteOptions>(options => options.LowercaseUrls = true);
            
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            if (env.IsProduction()) {  
                var options = new RewriteOptions();

                options.Add(new SSLRedirectRule());

                app.UseRewriter(options);
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}

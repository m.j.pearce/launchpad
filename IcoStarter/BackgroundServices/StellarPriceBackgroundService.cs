﻿using IcoStarter.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace IcoStarter.BackgroundServices
{
    [Inject(Lifetime.Singleton)]
    public class StellarPriceBackgroundService : BackgroundService
    {
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return Task.CompletedTask;
        }
    }
}

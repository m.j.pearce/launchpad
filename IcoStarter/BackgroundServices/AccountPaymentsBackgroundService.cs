﻿using IcoStarter.Common;
using IcoStarter.Common.Time;
using IcoStarter.Data;
using IcoStarter.HorizonClient;
using IcoStarter.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace IcoStarter.BackgroundServices
{
    [Inject(Lifetime.Singleton)]
    public class AccountPaymentsBackgroundService : BackgroundService
    {
        private readonly IBackgroundUnitOfWork _unitOfWork;
        private readonly ISettings _settings;
        private readonly IHorizonClient _horizonClient;
        private readonly ILogger<AccountPaymentsBackgroundService> _logger;

        public AccountPaymentsBackgroundService(IBackgroundUnitOfWork unitOfWork, 
            ISettings settings, 
            IHorizonClient horizonClient,
            ILogger<AccountPaymentsBackgroundService> logger)
        {
            _unitOfWork = unitOfWork;
            _settings = settings;
            _horizonClient = horizonClient;
            _logger = logger;
        }

        protected async override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogDebug($"{nameof(AccountPaymentsBackgroundService)} is starting.");

            stoppingToken.Register(() =>
                    _logger.LogDebug($"{nameof(AccountPaymentsBackgroundService)} is stopping."));

            while (!stoppingToken.IsCancellationRequested)
            {
                var nextTask = _unitOfWork.BackgroundTasks.GetNext<AccountPaymentsBackgroundService>();

                if (nextTask == null)
                {
                    // No tasks to be processed, wait a bit before rechecking
                    await Task.Delay(5000, stoppingToken);

                    continue;
                }

                double timeUntilTask = _settings.AccountPaymentsUpdateFrequencySeconds - (SystemTime.Now - nextTask.LastRun).TotalSeconds;

                if (timeUntilTask > 0.0)
                {
                    // Not quite time to run this task yet, delay for a bit until it is time to run
                    await Task.Delay((int)(timeUntilTask * 1000), stoppingToken);
                }

                var wallet = _unitOfWork.Wallets.GetWalletForProject(nextTask.ProjectId);

                var stellarAccount = wallet.PublicAddress;
                
                var payments = await _horizonClient
                    .GetPayments(stellarAccount, nextTask.HorizonCursor);

                if(payments?.Any() ?? false)
                {
                    foreach (var payment in payments.Where(p => p.IsLumens && p.To == stellarAccount))
                    {
                        _unitOfWork.Contributions.UpsertContributionAmount(nextTask.Project, payment.TransactionHash, payment.Amount);
                    }

                    nextTask.HorizonCursor = payments.Max(p => p.PagingToken);
                }
                
                nextTask.LastRun = DateTime.UtcNow;

                await _unitOfWork.SaveChanges();
            }

            _logger.LogDebug($"{nameof(AccountPaymentsBackgroundService)} has stopped.");
        }
    }
}

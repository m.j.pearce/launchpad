﻿using IcoStarter.Common;
using IcoStarter.Common.Time;
using IcoStarter.Data;
using IcoStarter.HorizonClient;
using IcoStarter.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace IcoStarter.BackgroundServices
{
    [Inject(Lifetime.Singleton)]
    public class AccountBalanceBackgroundWorker : BackgroundService
    {
        private readonly IBackgroundUnitOfWork _unitOfWork;
        private readonly ISettings _settings;
        private readonly IHorizonClient _horizonClient;
        private readonly ILogger<AccountPaymentsBackgroundService> _logger;

        public AccountBalanceBackgroundWorker(IBackgroundUnitOfWork unitOfWork, 
            ISettings settings, 
            IHorizonClient horizonClient,
            ILogger<AccountPaymentsBackgroundService> logger)
        {
            _unitOfWork = unitOfWork;
            _settings = settings;
            _horizonClient = horizonClient;
            _logger = logger;
        }

        protected async override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogDebug($"{nameof(AccountBalanceBackgroundWorker)} is starting.");

            stoppingToken.Register(() =>
                    _logger.LogDebug($"{nameof(AccountBalanceBackgroundWorker)} is stopping."));

            while (!stoppingToken.IsCancellationRequested)
            {
                var nextTask = _unitOfWork.BackgroundTasks.GetNext<AccountBalanceBackgroundWorker>();

                if (nextTask == null)
                {
                    // No tasks to be processed, wait a bit before rechecking
                    await Task.Delay(5000, stoppingToken);

                    continue;
                }

                double timeUntilTask = _settings.AccountBalanceUpdateFrequencySeconds - (SystemTime.Now - nextTask.LastRun).TotalSeconds;

                if (timeUntilTask > 0.0)
                {
                    // Not quite time to run this task yet, delay for a bit until it is time to run
                    await Task.Delay((int)(timeUntilTask * 1000), stoppingToken);
                }

                var wallet = _unitOfWork.Wallets.GetWalletForProject(nextTask.ProjectId);
                
                var account = await _horizonClient.GetAccount(wallet.PublicAddress);

                if(account != null)
                {
                    nextTask.Project.Raised = account.Balances
                        .Single(b => b.IsLumens)
                        .Balance;
                }

                // Save last run even if it wasn't successful, don't want to repeatedly try a broken account
                nextTask.LastRun = SystemTime.Now;

                await _unitOfWork.SaveChanges();
            }

            _logger.LogDebug($"{nameof(AccountBalanceBackgroundWorker)} has stopped.");
        }
    }
}

﻿using IcoStarter.Common;
using IcoStarter.Common.Time;
using IcoStarter.Data;
using IcoStarter.HorizonClient;
using IcoStarter.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace IcoStarter.BackgroundServices
{
    [Inject(Lifetime.Singleton)]
    public class AccountTransactionsBackgroundService : BackgroundService
    {
        private readonly IBackgroundUnitOfWork _unitOfWork;
        private readonly ISettings _settings;
        private readonly IHorizonClient _horizonClient;
        private readonly ILogger<AccountTransactionsBackgroundService> _logger;

        public AccountTransactionsBackgroundService(IBackgroundUnitOfWork unitOfWork, 
            ISettings settings, 
            IHorizonClient horizonClient,
            ILogger<AccountTransactionsBackgroundService> logger)
        {
            _unitOfWork = unitOfWork;
            _settings = settings;
            _horizonClient = horizonClient;
            _logger = logger;
        }

        protected async override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogDebug($"{nameof(AccountTransactionsBackgroundService)} is starting.");
            
            stoppingToken.Register(() =>
                    _logger.LogDebug($"{nameof(AccountTransactionsBackgroundService)} is stopping."));

            while (!stoppingToken.IsCancellationRequested)
            {
                var nextTask = _unitOfWork.BackgroundTasks.GetNext<AccountTransactionsBackgroundService>();

                if(nextTask == null)
                {
                    // No tasks to be processed, wait a bit before rechecking
                    await Task.Delay(5000, stoppingToken);

                    continue;
                }

                double timeUntilTask = _settings.AccountTransactionsUpdateFrequencySeconds - (SystemTime.Now - nextTask.LastRun).TotalSeconds;

                if (timeUntilTask > 0.0)
                {
                    // Not quite time to run this task yet, delay for a bit until it is time to run
                    await Task.Delay((int)(timeUntilTask * 1000), stoppingToken);
                }

                var wallet = _unitOfWork.Wallets.GetWalletForProject(nextTask.ProjectId);

                var transactions = await _horizonClient
                    .GetTransactions(wallet.PublicAddress, nextTask.HorizonCursor);

                if(transactions?.Any() ?? false)
                {
                    foreach (var transaction in transactions)
                    {
                        if (transaction.Memo == null || transaction.MemoType != "text")
                        {
                            // No memo, need to refund?
                            continue;
                        }

                        _unitOfWork.Contributions.UpsertContributionMemo(nextTask.Project, transaction.Id, transaction.Memo);
                    }

                    nextTask.HorizonCursor = transactions.Max(p => p.PagingToken);
                }

                nextTask.LastRun = DateTime.UtcNow;

                await _unitOfWork.SaveChanges();
            }

            _logger.LogDebug($"{nameof(AccountTransactionsBackgroundService)} has stopped.");
        }
    }
}

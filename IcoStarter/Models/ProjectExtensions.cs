﻿using IcoStarter.Common.Time;
using System;

namespace IcoStarter.Models
{
    public static class ProjectExtensions
    {
        public static string GetRaisedPercentage(this Project project)
        {
            int raisedPercentage = (int)(((double)project.Raised / (double)project.Target) * 100.0);

            if (raisedPercentage < 0)
            {
                raisedPercentage = 0;
            }

            if (raisedPercentage > 100)
            {
                raisedPercentage = 100;
            }

            return raisedPercentage.ToString();
        }

        public static string GetRaisedString(this Project project)
        {
            return string.Format("{0:#,##0}", project.Raised);
        }

        public static string GetTargetString(this Project project)
        {
            return string.Format("{0:#,##0}", project.Target);
        }

        public static string GetTimeRemaining(this Project project)
        {
            var calculator = new TimeLeftCalculator();

            calculator.Calculate(project.EndTime);

            if(calculator.Amount == 0) {
                if(project.Raised >= project.Target) 
                {
                    return "Finished (Successful)";
                } 
                else 
                {
                    return "Finished (Not successful)";
                }
            }

            return $"{calculator.Amount} {calculator.Units.ToString().ToLower()} remaining";
        }
    }
}

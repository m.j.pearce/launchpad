﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.Models
{
    public class ProjectFundingBonus
    {
        public int ProjectFundingBonusId { get; set; }

        public int ProjectId { get; set; }
        public Project Project { get; set; }

        public int FundingBonusId { get; set; }
        public FundingBonus FundingBonus { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.Models
{
    public class ProjectCurrency
    {
        public int ProjectCurrencyId { get; set; }

        public int ProjectId { get; set; }
        public Project Project { get; set; }

        public int CurrencyId { get; set; }
        public Currency Currency { get; set; }
    }
}

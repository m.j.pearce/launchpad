﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.Models
{
    public class Wallet
    {
        public int WalletId { get; set; }

        public int CurrencyId { get; set; }
        public Currency Currency { get; set; }

        public string PublicAddress { get; set; }

        public string EncryptedPrivateKey { get; set; }

        public string Salt { get; set; }

        public decimal Balance { get; set; }

        public DateTime UpdatedTime { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace IcoStarter.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        // Created projects
        public List<Project> Projects { get; set; }

        public List<Transaction> Contributions { get; set; }

        // Used to match transactions to users
        public string MemoValue { get; set; }
    }
}

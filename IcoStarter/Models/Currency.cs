﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.Models
{
    public class Currency
    {
        public int CurrencyId { get; set; }

        public string Name { get; set; }

        public string ShortCode { get; set; }

        public bool Visible { get; set; }

        public bool Enabled { get; set; }
    }
}

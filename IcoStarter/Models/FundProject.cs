﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.Models
{
    public class FundProject
    {
        public int FundProjectId { get; set; }

        public int FundId { get; set; }
        public Fund Fund { get; set; }

        public int ProjectId { get; set; }
        public Project Project { get; set; }

        public decimal Contribution { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.Models
{
    public class Transaction
    {
        public int TransactionId { get; set; }

        public string TransactionHash { get; set; }
        
        public int ProjectId { get; set; }
        public Project Project { get; set; }

        public decimal? Amount { get; set; }

        public string Memo { get; set; }
    }
}

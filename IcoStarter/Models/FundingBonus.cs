﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.Models
{
    public class FundingBonus
    {
        public int FundingBonusId { get; set; }

        public decimal StartPrice { get; set; }

        public decimal Multiplier { get; set; }
    }
}

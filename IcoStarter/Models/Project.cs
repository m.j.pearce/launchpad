﻿using System;
using System.Collections.Generic;

namespace IcoStarter.Models
{
    public class Project
    {
        public int ProjectId { get; set; }
        
        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string ShortName { get; set; }
        
        public string Content { get; set; }

        public decimal Target { get; set; }

        public decimal Raised { get; set; }

        public List<ProjectCurrency> Currencies { get; set; }

        public List<Wallet> Wallets { get; set; }

        public ProjectState State { get; set; }
        
        public DateTime CreatedTime { get; set; }
        public DateTime DeletedTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}

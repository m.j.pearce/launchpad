﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.Models
{
    public class BackgroundTask
    {
        public int BackgroundTaskId { get; set; }

        public int ProjectId { get; set; }
        public Project Project { get; set; }

        public string Worker { get; set; }
        
        public long HorizonCursor { get; set; }

        public DateTime LastRun { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.Models
{
    public static class WalletExtensions
    {
        public static Wallet GetStellarWallet(this Project project)
        {
            return project.Wallets.FirstOrDefault();
        }
    }
}

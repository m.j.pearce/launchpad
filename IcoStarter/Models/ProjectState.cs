﻿using System;
namespace IcoStarter.Models
{
    public enum ProjectState
    {
        Draft,
        Live,
        Ended
    }
}

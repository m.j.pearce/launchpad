﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.Models
{
    public class ExchangeRate
    {
        public int ExchangeRateId { get; set; }

        public int SourceCurrencyId { get; set; }
        [ForeignKey("SourceCurrencyId")]
        public Currency Source { get; set; }

        public int DestinationCurrencyId { get; set; }
        [ForeignKey("DestinationCurrencyId")]
        public Currency Destination { get; set; }

        public decimal Rate { get; set; }

        public DateTime UpdatedTime { get; set; }
    }
}

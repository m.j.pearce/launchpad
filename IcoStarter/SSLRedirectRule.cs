using System.Linq;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Extensions.Primitives;

public class SSLRedirectRule : IRule  
{
    public virtual void ApplyRule(RewriteContext context)
    {
        var request = context.HttpContext.Request;
        var response = context.HttpContext.Response;

        StringValues protocol;

        // Header filled by Heroku since reverse proxy strips protocol information
        if(request.Headers.TryGetValue("X-Forwarded-Proto", out protocol) && protocol.All(p => p != "https")) 
        {
            //response.Redirect("https://www.lpad.co" + request.Path);
        }
    }
}
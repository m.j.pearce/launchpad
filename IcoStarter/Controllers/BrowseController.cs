﻿using System.Linq;
using System.Threading.Tasks;
using IcoStarter.Data;
using IcoStarter.Models;
using IcoStarter.ViewModels.Browse;
using IcoStarter.ViewModels.Project;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace IcoStarter.Controllers
{
    public class BrowseController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<ApplicationUser> _userManager;

        public BrowseController(IUnitOfWork unitOfWork, UserManager<ApplicationUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
        }

        [HttpGet("browse")]
        public IActionResult Index()
        {
            var viewModel = new BrowseViewModel
            {
                Projects = _unitOfWork.Projects.ListLive()
            };

            return View(viewModel);
        }

        [HttpGet("/p/{shortName}")]
        public async Task<ActionResult> Details(string shortName)
        {
            var project = _unitOfWork.Projects.GetByShortName(shortName);

            if (project == null)
            {
                return NotFound();
            }

            var contributors = _unitOfWork.Contributions.GetUniqueProjectContributions(project.ProjectId);

            var userMemo = (await _userManager.GetUserAsync(HttpContext.User))?.MemoValue;

            var viewModel = new DetailsViewModel
            {
                Name = project.Name,
                ShortName = project.ShortName,
                Content = project.Content,
                Description = project.Description,
                AccountPublicKey = project.GetStellarWallet().PublicAddress,
                EndTime = project.EndTime,
                UserMemo = userMemo,
                Raised = (double)project.Raised,
                Target = (double)project.Target,
                Contributors = contributors
            };

            return View(viewModel);
        }
    }
}

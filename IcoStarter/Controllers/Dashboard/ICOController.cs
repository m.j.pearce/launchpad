﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using IcoStarter.BackgroundServices;
using IcoStarter.Common.Crypto;
using IcoStarter.Common.Extensions;
using IcoStarter.Common.Time;
using IcoStarter.Data;
using IcoStarter.Models;
using IcoStarter.Services;
using IcoStarter.ViewModels.Account;
using IcoStarter.ViewModels.Project;
using Markdig;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace IcoStarter.Controllers.Dashboard
{
    [Route("dashboard/icos")]
    public class ICOController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IImageUploadService _imageUploadService;

        public ICOController(IUnitOfWork unitOfWork, UserManager<ApplicationUser> userManager, IImageUploadService imageUploadService)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            _imageUploadService = imageUploadService;
        }

        [Authorize]
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            return View(new DashboardViewModel
            {
                Projects = _unitOfWork.Projects.GetStartedByUser(user)
            });
        }

        [Authorize]
        [HttpGet("new")]
        public ActionResult New()
        {
            return View("Edit");
        }
        
        [Authorize]
        [HttpGet("{shortName}/edit")]
        public async Task<ActionResult> Edit(string shortName)
        {
            var project = _unitOfWork.Projects.GetByShortName(shortName);

            if (project == null)
            {
                return NotFound();
            }

            var userId = (await _userManager.GetUserAsync(HttpContext.User)).Id;

            if (project.ApplicationUserId != userId)
            {
                return Unauthorized();
            }

            var viewModel = new EditProjectViewModel
            {
                ProjectId = project.ProjectId,
                Name = project.Name,
                Live = project.State == ProjectState.Live,
                ShortName = project.ShortName,
                Description = project.Description,
                Target = project.Target,
                EndTime = project.EndTime,
                Content = project.Content
                //LengthWeeks = project.LengthWeeks
            };

            return View("Edit", viewModel);
        }

        [Authorize]
        [HttpGet("{shortName}/launch")]
        public async Task<ActionResult> Launch(string shortName)
        {
            var project = _unitOfWork.Projects.GetByShortName(shortName);

            if (project == null)
            {
                return NotFound();
            }

            var userId = (await _userManager.GetUserAsync(HttpContext.User)).Id;

            if (project.ApplicationUserId != userId)
            {
                return Unauthorized();
            }

            project.State = ProjectState.Live;
            project.EndTime = SystemTime.Now.AddDays(7);

            await _unitOfWork.SaveChanges();

            return RedirectToAction("Details", "Browse", new { shortName = project.ShortName });
        }

        [Authorize]
        [HttpPost("update")]
        public async Task<ActionResult> Update(EditProjectViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Edit", model);
            }

            Project project;

            if (model.ProjectId == 0)
            {
                var keyPair = new KeyPair();

                project = new Project
                {
                    Name = model.Name,
                    Description = model.Description,
                    ShortName = model.Name.ToShortName(),
                    Target = model.Target,
                    CreatedTime = SystemTime.Now,
                    EndTime = model.EndTime,
                    ApplicationUserId = (await _userManager.GetUserAsync(HttpContext.User)).Id,
                    Wallets = new List<Wallet>
                    {
                        new Wallet
                        {
                            Currency = _unitOfWork.Currencies.GetCurrency("XLM"),
                            PublicAddress = keyPair.PublicKey,
                            EncryptedPrivateKey = keyPair.SecretKey,
                            UpdatedTime = SystemTime.Now
                        }
                    },

                    Raised = 0
                };

                _unitOfWork.Projects.Add(project);

                _unitOfWork.BackgroundTasks.Add<AccountBalanceBackgroundWorker>(project.ProjectId);
                _unitOfWork.BackgroundTasks.Add<AccountPaymentsBackgroundService>(project.ProjectId);
                _unitOfWork.BackgroundTasks.Add<AccountTransactionsBackgroundService>(project.ProjectId);
            }
            else
            {
                project = _unitOfWork.Projects.GetById(model.ProjectId);

                if (project == null)
                {
                    return NotFound();
                }

                var userId = (await _userManager.GetUserAsync(HttpContext.User)).Id;

                if (project.ApplicationUserId != userId)
                {
                    return Unauthorized();
                }
            }

            if (model.Logo != null)
            {
                var imageName = $"{project.ShortName}_logo";

                try
                {
                    await _imageUploadService.Upload(imageName, model.Logo.FileName.Split(".").Last().ToLower(), model.Logo.OpenReadStream());
                }
                catch (FileLoadException ex)
                {
                    ModelState.AddModelError("Logo", ex.Message);
                    return View("Edit", model);
                }
            }

            if (model.Banner != null)
            {
                var imageName = $"{project.ShortName}_banner";

                try
                {
                    await _imageUploadService.Upload(imageName, model.Banner.FileName.Split(".").Last().ToLower(), model.Banner.OpenReadStream());
                }
                catch (FileLoadException ex)
                {
                    ModelState.AddModelError("Banner", ex.Message);
                    return View("Edit", model);
                }
            }

            project.Content = model.Content;

            if (!await _unitOfWork.SaveChanges())
            {
                ModelState.AddModelError("Error", "An unexpected error occurred, please try again later.");

                return View("Edit", model);
            }

            return RedirectToAction("Details", "Browse", new { shortName = project.ShortName });
        }
    }
}
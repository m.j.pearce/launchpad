﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using IcoStarter.Common;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.Services
{
    [Inject]
    public class AWSImageUploadService : IImageUploadService
    {
        public async Task<string> Upload(string name, string extension, Stream imageStream)
        {
            var allowedExtensions = new[]
            {
                "jpg",
                "png"
            };

            if (!allowedExtensions.Contains(extension))
            {
                throw new FileLoadException($"File type .{extension} is not an allowed image type, please upload a .jpg or .png file.");
            }
            
            if (imageStream.Length > 5 * 1024 * 1024)
            {
                throw new FileLoadException($"Image file is too big ({imageStream.Length / 1024 / 1024} MB), please resize to less than 5MB.");
            }

            var s3Client = new AmazonS3Client(RegionEndpoint.USEast2);

            var request = new PutObjectRequest
            {
                BucketName = "lpad-assets",
                Key = name,
                ContentType = extension == "png" ? "image/png" : "image/jpeg",
                InputStream = imageStream,
                CannedACL = S3CannedACL.PublicRead
            };

            await s3Client.PutObjectAsync(request);

            return "";
        }
    }
}

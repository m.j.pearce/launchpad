﻿using System;
using System.Linq;
using IcoStarter.Common;

namespace IcoStarter.Services
{
    [Inject]
    public class MemoGenerator : IMemoGenerator
    {
        private readonly IRandomNumberSource _randomNumberSource;
        private const string _memoChars = "ABCDEFGHJKMNPQRSTUVWXYZ23456789";
        private const int _memoLength = 5;

        public MemoGenerator(IRandomNumberSource randomNumberSource)
        {
            _randomNumberSource = randomNumberSource;
        }

        public string GenerateMemo()
        {
            return string.Concat(_randomNumberSource.GetNumbers(0, _memoChars.Length - 1, _memoLength)
                                 .Select(i => _memoChars[i]));
        }
    }
}

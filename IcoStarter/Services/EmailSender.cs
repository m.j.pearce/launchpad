﻿using IcoStarter.Common;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace IcoStarter.Services
{
    [Inject]
    public class EmailSender : IEmailSender
    {
        const string domain = "sandboxbd196bc88708416fa0540135b8f77237.mailgun.org";

        public Task SendEmailAsync(string email, string subject, string message)
        {
            var client = new RestClient
            {
                BaseUrl = new Uri("https://api.mailgun.net/v3"),
                Authenticator = new HttpBasicAuthenticator("api", "key-6045da03c2d67241da7c5972f23e9655")
            };

            var request = new RestRequest();
            request.AddParameter("domain", domain, ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", $"IcoStarter <postmaster@{domain}>");
            request.AddParameter("to", email);
            request.AddParameter("subject", subject);
            request.AddParameter("html", message);
            request.Method = Method.POST;

            client.Execute(request);

            return Task.CompletedTask;
        }
    }
}

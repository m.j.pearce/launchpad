﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.Services
{
    public interface IImageUploadService
    {
        Task<string> Upload(string name, string extension, Stream imageStream);
    }
}

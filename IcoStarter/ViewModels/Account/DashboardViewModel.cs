﻿using IcoStarter.Models;
using System;
using System.Collections.Generic;

namespace IcoStarter.ViewModels.Account
{
    public class DashboardViewModel
    {
        public ApplicationUser User { get; set; }
        
        public List<Models.Project> Projects { get; set; }
    }
}

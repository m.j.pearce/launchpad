﻿using IcoStarter.Common.Time;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.ViewModels.Project
{
    public class DetailsViewModel
    {
        public string Name { get; set; }

        public string ShortName { get; set; }

        public string Description { get; set; }

        public int Contributors { get; set; }

        public string Content { get; set; }

        public string AccountPublicKey { get; set; }

        public double Raised { get; set; }

        public double Target { get; set; }

        public string UserMemo { get; set; }
        
        private DateTime _endTime;

        public DateTime EndTime
        {
            get
            {
                return _endTime;
            }
            set
            {
                _endTime = value;

                _timeLeftCalculator.Calculate(_endTime);
            }
        }

        private TimeLeftCalculator _timeLeftCalculator = new TimeLeftCalculator();

        public int TimeRemainingAmount => _timeLeftCalculator.Amount;

        public string TimeRemainingUnits => _timeLeftCalculator.Units.ToString();
    }
}

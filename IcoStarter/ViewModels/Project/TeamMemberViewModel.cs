﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.ViewModels.Project
{
    public class TeamMemberViewModel
    {
        public string Name { get; set; }

        public string Role { get; set; }

        public IFormFile Profile { get; set; }
    }
}

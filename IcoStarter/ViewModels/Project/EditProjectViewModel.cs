﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace IcoStarter.ViewModels.Project
{
    public class EditProjectViewModel
    {
        public int ProjectId { get; set; }

        [Required]
        public string Name { get; set; }

        public bool Live { get; set; }

        public string ShortName { get; set; }

        [Required]
        public string Description { get; set; }

        public IFormFile Logo { get; set; }

        public IFormFile Banner { get; set; }

        [Required]
        public string Content { get; set; }

        [Required]
        [Range(5000.0, 1000000.0)]
        public decimal Target { get; set; }

        [Required]
        public DateTime EndTime { get; set; }

        public List<TeamMemberViewModel> TeamMembers { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.ViewModels.Browse
{
    public class BrowseViewModel
    {
        public List<Models.Project> Projects { get; set; }
    }
}

﻿using IcoStarter.Models;

namespace IcoStarter.Data
{
    public interface ICurrencyRepository
    {
        Currency GetCurrency(string symbol);
    }
}
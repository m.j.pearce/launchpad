﻿using IcoStarter.Common;
using IcoStarter.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.Data
{
    [Inject]
    public class ProjectRepository : IProjectRepository
    {
        private ApplicationDbContext _applicationDbContext;

        public ProjectRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public void Add(Project project)
        {
            _applicationDbContext.Projects.Add(project);
        }

        public Project GetById(int id)
        {
            return _applicationDbContext.Projects
                .FirstOrDefault(c => c.ProjectId == id);
        }

        public Project GetByShortName(string shortName)
        {
            return _applicationDbContext.Projects
                .Include(p => p.Wallets)
                .FirstOrDefault(c => c.ShortName == shortName);
        }
        
        public List<Project> List()
        {
            return _applicationDbContext.Projects.ToList();
        }

        public List<Project> ListLive()
        {
            return _applicationDbContext.Projects
                                        .Where(p => p.State == ProjectState.Live)
                                        .ToList();
        }

        public List<Project> GetStartedByUser(ApplicationUser user)
        {
            return _applicationDbContext.Projects
                                        .Where(p => p.ApplicationUserId == user.Id)
                                        .Include(p => p.Wallets)
                                        .ToList();
        }
    }
}

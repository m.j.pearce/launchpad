﻿using IcoStarter.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.Data
{
    public class WalletRepository : IWalletRepository
    {
        private ApplicationDbContext _applicationDbContext;

        public WalletRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public Wallet GetWalletForProject(int projectId)
        {
            return _applicationDbContext.Projects
                .Include(p => p.Wallets)
                .FirstOrDefault(p => p.ProjectId == projectId)
                ?.Wallets.FirstOrDefault();
        }
    }
}

﻿using IcoStarter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.Data
{
    public interface IContributionRepository
    {
        void UpsertContributionMemo(Project project, string transactionHash, string memo);

        void UpsertContributionAmount(Project project, string transactionHash, decimal amount);

        int GetUniqueProjectContributions(int projectId);

        List<Transaction> GetContributionsWithMemo(string memo);
    }
}

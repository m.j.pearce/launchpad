﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.Data
{
    // Will be scoped per-request
    public interface IUnitOfWork
    {
        ICurrencyRepository Currencies { get; }

        IProjectRepository Projects { get; }

        IContributionRepository Contributions { get; }

        IUserRepository Users { get; }
        
        IBackgroundTaskRepository BackgroundTasks { get; }

        IWalletRepository Wallets { get; }

        Task<bool> SaveChanges();
    }
}

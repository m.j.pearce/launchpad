﻿using IcoStarter.Common;
using IcoStarter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.Data
{
    [Inject]
    public class UserRepository : IUserRepository
    {
        private ApplicationDbContext _applicationDbContext;

        public UserRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public ApplicationUser GetByMemo(string memo)
        {
            return _applicationDbContext.Users.SingleOrDefault(u => u.MemoValue == memo);
        }
    }
}

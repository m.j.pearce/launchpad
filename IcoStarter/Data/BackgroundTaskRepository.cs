﻿using IcoStarter.Common;
using IcoStarter.Common.Time;
using IcoStarter.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.Data
{
    [Inject]
    public class BackgroundTaskRepository : IBackgroundTaskRepository
    {
        private ApplicationDbContext _applicationDbContext;

        public BackgroundTaskRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public BackgroundTask GetNext<TBackgroundWorker>()
        {
            var worker = typeof(TBackgroundWorker).FullName;
            var currentTime = SystemTime.Now;

            return _applicationDbContext.BackgroundTasks
                .OrderBy(t => t.LastRun)
                .Include(t => t.Project)
                .FirstOrDefault(t => t.Worker == worker);
        }

        public void Add<TBackgroundWorker>(int projectId)
        {
            var worker = typeof(TBackgroundWorker).FullName;

            var backgroundTask = new BackgroundTask
            {
                LastRun = DateTime.MinValue,
                ProjectId = projectId,
                Worker = worker
            };

            _applicationDbContext.BackgroundTasks.Add(backgroundTask);
        }
    }
}

﻿using IcoStarter.Common;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.Data
{
    // No [Inject] attribute here as this is manually registered with different lifetimes
    // depending on the reference interface
    public class UnitOfWork : IUnitOfWork, IBackgroundUnitOfWork
    {
        private ApplicationDbContext _applicationDbContext;
        private ILogger<UnitOfWork> _logger;

        public UnitOfWork(ApplicationDbContext applicationDbContext, ILogger<UnitOfWork> logger)
        {
            _applicationDbContext = applicationDbContext;
            _logger = logger;
        }

        private IProjectRepository _projects;
        public IProjectRepository Projects => _projects = _projects ?? new ProjectRepository(_applicationDbContext);

        private IContributionRepository _contributions;
        public IContributionRepository Contributions => _contributions = _contributions ?? new ContributionRepository(_applicationDbContext);

        private IBackgroundTaskRepository _backgroundTasks;
        public IBackgroundTaskRepository BackgroundTasks => _backgroundTasks = _backgroundTasks ?? new BackgroundTaskRepository(_applicationDbContext);

        private IUserRepository _users;
        public IUserRepository Users => _users = _users ?? new UserRepository(_applicationDbContext);

        private ICurrencyRepository _currencies;
        public ICurrencyRepository Currencies => _currencies = _currencies ?? new CurrencyRepository(_applicationDbContext);

        private IWalletRepository _wallets;
        public IWalletRepository Wallets => _wallets = _wallets ?? new WalletRepository(_applicationDbContext);

        public async Task<bool> SaveChanges()
        {
            try
            {
                await _applicationDbContext.SaveChangesAsync();

                return true;
            }
            catch (Exception e)
            {
                _logger.LogError($"Exception while saving database changes: {e.Message}");

                return false;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IcoStarter.Models;

namespace IcoStarter.Data
{
    public interface IProjectRepository
    {
        void Add(Project project);

        Project GetById(int id);

        Project GetByShortName(string shortName);
        
        List<Project> List();

        List<Project> GetStartedByUser(ApplicationUser user);

		List<Project> ListLive();
	}
}

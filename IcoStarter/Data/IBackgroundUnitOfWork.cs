﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.Data
{
    // Will be scoped transient
    public interface IBackgroundUnitOfWork : IUnitOfWork
    {
    }
}

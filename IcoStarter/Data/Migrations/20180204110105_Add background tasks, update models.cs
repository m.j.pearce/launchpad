﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace IcoStarter.Data.Migrations
{
    public partial class Addbackgroundtasksupdatemodels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "Raised",
                table: "Campaigns",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "MemoValue",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BackgroundTasks",
                columns: table => new
                {
                    BackgroundTaskId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CampaignId = table.Column<int>(nullable: false),
                    HorizonCursor = table.Column<long>(nullable: false),
                    LastRun = table.Column<DateTime>(nullable: false),
                    Worker = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BackgroundTasks", x => x.BackgroundTaskId);
                    table.ForeignKey(
                        name: "FK_BackgroundTasks_Campaigns_CampaignId",
                        column: x => x.CampaignId,
                        principalTable: "Campaigns",
                        principalColumn: "CampaignId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Contribution",
                columns: table => new
                {
                    ContributionId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Amount = table.Column<decimal>(nullable: true),
                    ApplicationUserId = table.Column<string>(nullable: true),
                    CampaignId = table.Column<int>(nullable: false),
                    TransactionHash = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contribution", x => x.ContributionId);
                    table.ForeignKey(
                        name: "FK_Contribution_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Contribution_Campaigns_CampaignId",
                        column: x => x.CampaignId,
                        principalTable: "Campaigns",
                        principalColumn: "CampaignId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_MemoValue",
                table: "AspNetUsers",
                column: "MemoValue",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BackgroundTasks_CampaignId",
                table: "BackgroundTasks",
                column: "CampaignId");

            migrationBuilder.CreateIndex(
                name: "IX_Contribution_ApplicationUserId",
                table: "Contribution",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Contribution_CampaignId",
                table: "Contribution",
                column: "CampaignId");

            migrationBuilder.CreateIndex(
                name: "IX_Contribution_TransactionHash",
                table: "Contribution",
                column: "TransactionHash",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BackgroundTasks");

            migrationBuilder.DropTable(
                name: "Contribution");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_MemoValue",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Raised",
                table: "Campaigns");

            migrationBuilder.DropColumn(
                name: "MemoValue",
                table: "AspNetUsers");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace IcoStarter.Data.Migrations
{
    public partial class Changecontributionstotrackingmemoinsteadofusers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contribution_AspNetUsers_ApplicationUserId",
                table: "Contribution");

            migrationBuilder.DropForeignKey(
                name: "FK_Contribution_Campaigns_CampaignId",
                table: "Contribution");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Contribution",
                table: "Contribution");

            migrationBuilder.RenameTable(
                name: "Contribution",
                newName: "Contributions");

            migrationBuilder.RenameIndex(
                name: "IX_Contribution_TransactionHash",
                table: "Contributions",
                newName: "IX_Contributions_TransactionHash");

            migrationBuilder.RenameIndex(
                name: "IX_Contribution_CampaignId",
                table: "Contributions",
                newName: "IX_Contributions_CampaignId");

            migrationBuilder.RenameIndex(
                name: "IX_Contribution_ApplicationUserId",
                table: "Contributions",
                newName: "IX_Contributions_ApplicationUserId");

            migrationBuilder.AddColumn<string>(
                name: "Memo",
                table: "Contributions",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Contributions",
                table: "Contributions",
                column: "ContributionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Contributions_AspNetUsers_ApplicationUserId",
                table: "Contributions",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Contributions_Campaigns_CampaignId",
                table: "Contributions",
                column: "CampaignId",
                principalTable: "Campaigns",
                principalColumn: "CampaignId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contributions_AspNetUsers_ApplicationUserId",
                table: "Contributions");

            migrationBuilder.DropForeignKey(
                name: "FK_Contributions_Campaigns_CampaignId",
                table: "Contributions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Contributions",
                table: "Contributions");

            migrationBuilder.DropColumn(
                name: "Memo",
                table: "Contributions");

            migrationBuilder.RenameTable(
                name: "Contributions",
                newName: "Contribution");

            migrationBuilder.RenameIndex(
                name: "IX_Contributions_TransactionHash",
                table: "Contribution",
                newName: "IX_Contribution_TransactionHash");

            migrationBuilder.RenameIndex(
                name: "IX_Contributions_CampaignId",
                table: "Contribution",
                newName: "IX_Contribution_CampaignId");

            migrationBuilder.RenameIndex(
                name: "IX_Contributions_ApplicationUserId",
                table: "Contribution",
                newName: "IX_Contribution_ApplicationUserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Contribution",
                table: "Contribution",
                column: "ContributionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Contribution_AspNetUsers_ApplicationUserId",
                table: "Contribution",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Contribution_Campaigns_CampaignId",
                table: "Contribution",
                column: "CampaignId",
                principalTable: "Campaigns",
                principalColumn: "CampaignId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

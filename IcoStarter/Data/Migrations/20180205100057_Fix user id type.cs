﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace IcoStarter.Data.Migrations
{
    public partial class Fixuseridtype : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Campaigns_AspNetUsers_ApplicationUserId1",
                table: "Campaigns");

            migrationBuilder.DropIndex(
                name: "IX_Campaigns_ApplicationUserId1",
                table: "Campaigns");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId1",
                table: "Campaigns");

            migrationBuilder.AlterColumn<string>(
                name: "ApplicationUserId",
                table: "Campaigns",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_Campaigns_ApplicationUserId",
                table: "Campaigns",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Campaigns_AspNetUsers_ApplicationUserId",
                table: "Campaigns",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Campaigns_AspNetUsers_ApplicationUserId",
                table: "Campaigns");

            migrationBuilder.DropIndex(
                name: "IX_Campaigns_ApplicationUserId",
                table: "Campaigns");

            migrationBuilder.AlterColumn<int>(
                name: "ApplicationUserId",
                table: "Campaigns",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId1",
                table: "Campaigns",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Campaigns_ApplicationUserId1",
                table: "Campaigns",
                column: "ApplicationUserId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Campaigns_AspNetUsers_ApplicationUserId1",
                table: "Campaigns",
                column: "ApplicationUserId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace IcoStarter.Data.Migrations
{
    public partial class RenameCampaignToProject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BackgroundTasks_Campaigns_CampaignId",
                table: "BackgroundTasks");

            migrationBuilder.DropForeignKey(
                name: "FK_Contributions_Campaigns_CampaignId",
                table: "Contributions");

            migrationBuilder.DropForeignKey(
                name: "FK_Goal_Campaigns_CampaignId",
                table: "Goal");

            migrationBuilder.DropForeignKey(
                name: "FK_Poll_Campaigns_CampaignId",
                table: "Poll");

            migrationBuilder.DropForeignKey(
                name: "FK_Reward_Campaigns_CampaignId",
                table: "Reward");

            migrationBuilder.DropTable(
                name: "Campaigns");

            migrationBuilder.RenameColumn(
                name: "CampaignId",
                table: "Reward",
                newName: "ProjectId");

            migrationBuilder.RenameIndex(
                name: "IX_Reward_CampaignId",
                table: "Reward",
                newName: "IX_Reward_ProjectId");

            migrationBuilder.RenameColumn(
                name: "CampaignId",
                table: "Poll",
                newName: "ProjectId");

            migrationBuilder.RenameIndex(
                name: "IX_Poll_CampaignId",
                table: "Poll",
                newName: "IX_Poll_ProjectId");

            migrationBuilder.RenameColumn(
                name: "CampaignId",
                table: "Goal",
                newName: "ProjectId");

            migrationBuilder.RenameIndex(
                name: "IX_Goal_CampaignId",
                table: "Goal",
                newName: "IX_Goal_ProjectId");

            migrationBuilder.RenameColumn(
                name: "CampaignId",
                table: "Contributions",
                newName: "ProjectId");

            migrationBuilder.RenameIndex(
                name: "IX_Contributions_CampaignId",
                table: "Contributions",
                newName: "IX_Contributions_ProjectId");

            migrationBuilder.RenameColumn(
                name: "CampaignId",
                table: "BackgroundTasks",
                newName: "ProjectId");

            migrationBuilder.RenameIndex(
                name: "IX_BackgroundTasks_CampaignId",
                table: "BackgroundTasks",
                newName: "IX_BackgroundTasks_ProjectId");

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    ProjectId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    AccountEncryptedSeed = table.Column<string>(nullable: true),
                    AccountPublicKey = table.Column<string>(nullable: true),
                    ApplicationUserId = table.Column<string>(nullable: true),
                    Content = table.Column<string>(nullable: true),
                    CreatedTime = table.Column<DateTime>(nullable: false),
                    DeletedTime = table.Column<DateTime>(nullable: false),
                    EndTime = table.Column<DateTime>(nullable: false),
                    IconUri = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Raised = table.Column<decimal>(nullable: false),
                    ShortName = table.Column<string>(nullable: true),
                    Target = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.ProjectId);
                    table.ForeignKey(
                        name: "FK_Projects_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Projects_ApplicationUserId",
                table: "Projects",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_BackgroundTasks_Projects_ProjectId",
                table: "BackgroundTasks",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "ProjectId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Contributions_Projects_ProjectId",
                table: "Contributions",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "ProjectId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Goal_Projects_ProjectId",
                table: "Goal",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "ProjectId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Poll_Projects_ProjectId",
                table: "Poll",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "ProjectId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Reward_Projects_ProjectId",
                table: "Reward",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "ProjectId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BackgroundTasks_Projects_ProjectId",
                table: "BackgroundTasks");

            migrationBuilder.DropForeignKey(
                name: "FK_Contributions_Projects_ProjectId",
                table: "Contributions");

            migrationBuilder.DropForeignKey(
                name: "FK_Goal_Projects_ProjectId",
                table: "Goal");

            migrationBuilder.DropForeignKey(
                name: "FK_Poll_Projects_ProjectId",
                table: "Poll");

            migrationBuilder.DropForeignKey(
                name: "FK_Reward_Projects_ProjectId",
                table: "Reward");

            migrationBuilder.DropTable(
                name: "Projects");

            migrationBuilder.RenameColumn(
                name: "ProjectId",
                table: "Reward",
                newName: "CampaignId");

            migrationBuilder.RenameIndex(
                name: "IX_Reward_ProjectId",
                table: "Reward",
                newName: "IX_Reward_CampaignId");

            migrationBuilder.RenameColumn(
                name: "ProjectId",
                table: "Poll",
                newName: "CampaignId");

            migrationBuilder.RenameIndex(
                name: "IX_Poll_ProjectId",
                table: "Poll",
                newName: "IX_Poll_CampaignId");

            migrationBuilder.RenameColumn(
                name: "ProjectId",
                table: "Goal",
                newName: "CampaignId");

            migrationBuilder.RenameIndex(
                name: "IX_Goal_ProjectId",
                table: "Goal",
                newName: "IX_Goal_CampaignId");

            migrationBuilder.RenameColumn(
                name: "ProjectId",
                table: "Contributions",
                newName: "CampaignId");

            migrationBuilder.RenameIndex(
                name: "IX_Contributions_ProjectId",
                table: "Contributions",
                newName: "IX_Contributions_CampaignId");

            migrationBuilder.RenameColumn(
                name: "ProjectId",
                table: "BackgroundTasks",
                newName: "CampaignId");

            migrationBuilder.RenameIndex(
                name: "IX_BackgroundTasks_ProjectId",
                table: "BackgroundTasks",
                newName: "IX_BackgroundTasks_CampaignId");

            migrationBuilder.CreateTable(
                name: "Campaigns",
                columns: table => new
                {
                    CampaignId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    AccountEncryptedSeed = table.Column<string>(nullable: true),
                    AccountPublicKey = table.Column<string>(nullable: true),
                    ApplicationUserId = table.Column<string>(nullable: true),
                    Content = table.Column<string>(nullable: true),
                    CreatedTime = table.Column<DateTime>(nullable: false),
                    DeletedTime = table.Column<DateTime>(nullable: false),
                    EndTime = table.Column<DateTime>(nullable: false),
                    IconUri = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Raised = table.Column<decimal>(nullable: false),
                    ShortName = table.Column<string>(nullable: true),
                    Target = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Campaigns", x => x.CampaignId);
                    table.ForeignKey(
                        name: "FK_Campaigns_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Campaigns_ApplicationUserId",
                table: "Campaigns",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_BackgroundTasks_Campaigns_CampaignId",
                table: "BackgroundTasks",
                column: "CampaignId",
                principalTable: "Campaigns",
                principalColumn: "CampaignId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Contributions_Campaigns_CampaignId",
                table: "Contributions",
                column: "CampaignId",
                principalTable: "Campaigns",
                principalColumn: "CampaignId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Goal_Campaigns_CampaignId",
                table: "Goal",
                column: "CampaignId",
                principalTable: "Campaigns",
                principalColumn: "CampaignId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Poll_Campaigns_CampaignId",
                table: "Poll",
                column: "CampaignId",
                principalTable: "Campaigns",
                principalColumn: "CampaignId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Reward_Campaigns_CampaignId",
                table: "Reward",
                column: "CampaignId",
                principalTable: "Campaigns",
                principalColumn: "CampaignId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

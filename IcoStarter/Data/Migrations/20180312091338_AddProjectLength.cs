﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace IcoStarter.Data.Migrations
{
    public partial class AddProjectLength : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IconUri",
                table: "Projects");

            migrationBuilder.AddColumn<int>(
                name: "LengthWeeks",
                table: "Projects",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "Live",
                table: "Projects",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LengthWeeks",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "Live",
                table: "Projects");

            migrationBuilder.AddColumn<string>(
                name: "IconUri",
                table: "Projects",
                nullable: true);
        }
    }
}

﻿using IcoStarter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.Data
{
    public interface IUserRepository
    {
        ApplicationUser GetByMemo(string memo);
    }
}

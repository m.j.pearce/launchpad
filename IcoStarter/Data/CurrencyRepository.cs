﻿using IcoStarter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcoStarter.Data
{
    public class CurrencyRepository : ICurrencyRepository
    {
        private ApplicationDbContext _applicationDbContext;

        public CurrencyRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public Currency GetCurrency(string symbol)
        {
            var currency = _applicationDbContext.Currencies.FirstOrDefault(c => c.ShortCode == symbol);

            if(currency == null)
            {
                currency = new Currency
                {
                    Name = "Stellar Lumens",
                    ShortCode = "XLM",
                    Visible = true,
                    Enabled = true
                };

                _applicationDbContext.Currencies.Add(currency);

                _applicationDbContext.SaveChanges();
            }

            return currency;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IcoStarter.Common;
using IcoStarter.Models;
using Microsoft.EntityFrameworkCore;
using Npgsql;

namespace IcoStarter.Data
{
    [Inject]
    public class ContributionRepository : IContributionRepository
    {
        private ApplicationDbContext _applicationDbContext;

        public ContributionRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public void UpsertContributionAmount(Project project, string transactionHash, decimal amount)
        {
            _applicationDbContext.Database.ExecuteSqlCommand(@"
                INSERT INTO ""Contributions""(""ProjectId"", ""TransactionHash"", ""Amount"")
                VALUES({0}, {1}, {2})
                ON CONFLICT (""TransactionHash"") DO UPDATE
                SET ""Amount""={2}", project.ProjectId, transactionHash, amount);
        }

        public void UpsertContributionMemo(Project project, string transactionHash, string memo)
        {
            _applicationDbContext.Database.ExecuteSqlCommand(@"
                INSERT INTO ""Contributions""(""ProjectId"", ""TransactionHash"", ""Memo"")
                VALUES({0}, {1}, {2})
                ON CONFLICT (""TransactionHash"") DO UPDATE
                SET ""Memo""={2}", project.ProjectId, transactionHash, memo);
        }

        public int GetUniqueProjectContributions(int projectId)
        {
            return _applicationDbContext.Contributions
                .Where(c => c.ProjectId == projectId)
                .Select(c => c.Memo)
                .Distinct()
                .Count();
        }

        public List<Transaction> GetContributionsWithMemo(string memo)
        {
            return _applicationDbContext.Contributions
                                        .Where(c => c.Memo == memo)
                                        .ToList();
        }
    }
}

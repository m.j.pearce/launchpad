﻿using IcoStarter.Common;
using IcoStarter.Common.HTTP;
using IcoStarter.HorizonClient.RestApi;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IcoStarter.HorizonClient
{
    [Inject]
    public class HorizonClient : IHorizonClient
    {
        private readonly ISettings _settings;
        private readonly IHttpClient _httpClient;

        public HorizonClient(ISettings settings, IHttpClient httpClient)
        {
            _settings = settings;
            _httpClient = httpClient;
        }

        public async Task<Account> GetAccount(string accountPublicKey)
        {
            var accountUri = $"{_settings.HorizonUri}/accounts/{accountPublicKey}";

            return await GetResponse<Account>(accountUri);
        }

        public async Task<List<Transaction>> GetTransactions(string accountPublicKey, long cursor)
        {
            var transactionsUri = $"{_settings.HorizonUri}/accounts/{accountPublicKey}/transactions?cursor={cursor}&limit={_settings.HorizonPageSize}";

            return (await GetResponse<ListResponse<Transaction>>(transactionsUri))?.Embedded?.Records;
        }

        public async Task<List<Payment>> GetPayments(string accountPublicKey, long cursor)
        {
            var paymentsUri = $"{_settings.HorizonUri}/accounts/{accountPublicKey}/payments?cursor={cursor}&limit={_settings.HorizonPageSize}";
            
            return (await GetResponse<ListResponse<Payment>>(paymentsUri))?.Embedded?.Records;
        }

        private async Task<TResponse> GetResponse<TResponse>(string uri)
            where TResponse : class
        {
            var response = await _httpClient.GetAsync(uri);
            
            return response != null ? JsonConvert.DeserializeObject<TResponse>(response) : (TResponse)null;
        }
    }
}

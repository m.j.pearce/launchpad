﻿using IcoStarter.HorizonClient.RestApi;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IcoStarter.HorizonClient
{
    public interface IHorizonClient
    {
        Task<Account> GetAccount(string accountPublicKey);

        Task<List<Transaction>> GetTransactions(string accountPublicKey, long cursor);

        Task<List<Payment>> GetPayments(string accountPublicKey, long cursor);
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IcoStarter.HorizonClient.RestApi
{
    public class AccountBalance
    {
        public decimal Balance { get; set; }

        [JsonProperty("asset_type")]
        public string AssetType { get; set; }

        [JsonIgnore]
        public bool IsLumens => AssetType == "native";
    }
}

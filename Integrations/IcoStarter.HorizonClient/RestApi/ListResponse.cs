﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IcoStarter.HorizonClient.RestApi
{
    public class ListResponse<TEmbeddedType>
    {
        [JsonProperty("_embedded")]
        public Embedded<TEmbeddedType> Embedded { get; set; }
    }
}

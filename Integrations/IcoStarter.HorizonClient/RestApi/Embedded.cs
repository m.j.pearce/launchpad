﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IcoStarter.HorizonClient.RestApi
{
    public class Embedded<TRecordType>
    {
        public List<TRecordType> Records { get; set; }
    }
}

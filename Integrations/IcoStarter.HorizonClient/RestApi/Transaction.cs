﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IcoStarter.HorizonClient.RestApi
{
    public class Transaction
    {
        public string Id { get; set; }

        [JsonProperty("paging_token")]
        public long PagingToken { get; set; }

        public string Hash { get; set; }

        public long Ledger { get; set; }

        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("source_account")]
        public string SourceAccount { get; set; }

        [JsonProperty("source_account_sequence")]
        public long SourceAccountSequence { get; set; }

        [JsonProperty("fee_paid")]
        public long FeePaid { get; set; }

        [JsonProperty("operation_count")]
        public long OperationCount { get; set; }

        [JsonProperty("memo_type")]
        public string MemoType { get; set; }

        public string Memo { get; set; }

        public List<string> Signatures { get; set; }
    }
}

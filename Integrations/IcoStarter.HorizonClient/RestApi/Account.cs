﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IcoStarter.HorizonClient.RestApi
{
    public class Account
    {
        public string Id { get; set; }

        [JsonProperty("account_id")]
        public string AccountId { get; set; }

        public long Sequence { get; set; }

        [JsonProperty("inflation_destination")]
        public string InflationDestination { get; set; }

        public List<AccountBalance> Balances { get; set; }
    }
}

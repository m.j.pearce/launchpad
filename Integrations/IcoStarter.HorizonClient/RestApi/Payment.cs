﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IcoStarter.HorizonClient.RestApi
{
    public class Payment
    {
        public long Id { get; set; }

        [JsonProperty("paging_token")]
        public long PagingToken { get; set; }

        [JsonProperty("source_account")]
        public string SourceAccount { get; set; }

        public string Type { get; set; }

        [JsonProperty("type_i")]
        public int TypeIndex { get; set; }

        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("transaction_hash")]
        public string TransactionHash { get; set; }

        [JsonProperty("asset_type")]
        public string AssetType { get; set; }

        [JsonIgnore]
        public bool IsLumens => AssetType == "native";

        public string From { get; set; }

        public string To { get; set; }

        public decimal Amount { get; set; }
    }
}

﻿using IcoStarter.Common;
using IcoStarter.Common.HTTP;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcoStarter.HorizonClient.Tests
{
    public class HorizonClientTest
    {
        private HorizonClient _client;

        private Mock<ISettings> _settingsMock;
        private Mock<IHttpClient> _httpClientMock;

        [SetUp]
        public void SetUp()
        {
            _settingsMock = new Mock<ISettings>();

            _settingsMock.SetupGet(s => s.HorizonUri).Returns("http://api.test.com");
            _settingsMock.SetupGet(s => s.HorizonPageSize).Returns(5);

            _httpClientMock = new Mock<IHttpClient>();

            _client = new HorizonClient(_settingsMock.Object, _httpClientMock.Object);

        }

        [Test]
        public async Task AccountResponse_IsParsedCorrectly()
        {
            // Arrange
            var expectedUri = $"http://api.test.com/abcdef12345";
            var exampleResponse = System.IO.File.ReadAllText("ExampleResponses/Account.json");

            _httpClientMock.Setup(h => h.GetAsync(expectedUri)).ReturnsAsync(exampleResponse);

            // Act
            var account = await _client.GetAccount("abcdef12345");

            // Assert
            Assert.Multiple(() =>
            {
                Assert.AreEqual("GA2VOQDFVRJZZAO7G247YCM6RZJH7WSD2DQDLJ5EDMBSHI6T5IFXLGU4", account.Id);
                Assert.AreEqual("GA2VOQDFVRJZZAO7G247YCM6RZJH7WSD2DQDLJ5EDMBSHI6T5IFXLGU4", account.AccountId);
                Assert.AreEqual(66777176560631818, account.Sequence);
                Assert.AreEqual("GA3FUYFOPWZ25YXTCA73RK2UGONHCO27OHQRSGV3VCE67UEPEFEDCOPA", account.InflationDestination);
                Assert.AreEqual(40.9677721, account.Balances.Single().Balance);
                Assert.AreEqual("native", account.Balances.Single().AssetType);
            });
        }

        [Test]
        public async Task TransactionsResponse_IsParsedCorrectly()
        {
            // Arrange
            var expectedUri = $"http://api.test.com/abcdef12345/transactions?cursor=20&limit=5";
            var exampleResponse = System.IO.File.ReadAllText("ExampleResponses/Transactions.json");

            _httpClientMock.Setup(h => h.GetAsync(expectedUri)).ReturnsAsync(exampleResponse);

            // Act
            var transactions = await _client.GetTransactions("abcdef12345", 20);

            // Assert
            Assert.AreEqual(4, transactions.Count);

            Assert.Multiple(() =>
            {
                Assert.AreEqual("10/01/2018 3:32:38 AM", transactions[0].CreatedAt.ToString());
                Assert.AreEqual(100, transactions[0].FeePaid);
                Assert.AreEqual("232d16e3240f96cd110887ed6697f667d232d36b91197ae2fee7ccd1137deec6", transactions[0].Hash);
                Assert.AreEqual("232d16e3240f96cd110887ed6697f667d232d36b91197ae2fee7ccd1137deec6", transactions[0].Id);
                Assert.AreEqual(15549802, transactions[0].Ledger);
                Assert.AreEqual("1041130464", transactions[0].Memo);
                Assert.AreEqual("text", transactions[0].MemoType);
                Assert.AreEqual(1, transactions[0].OperationCount);
                Assert.AreEqual(66785891049287680, transactions[0].PagingToken);
                Assert.AreEqual("dB4/PdYSBP74KMkA62B/lujXOP5wQgmgadobsXulbBCM6zStcjl0/GV8hXAc5paFcLH+AaqoOfEDHM2L3LdCCQ==", transactions[0].Signatures.Single());
                Assert.AreEqual("GA2VOQDFVRJZZAO7G247YCM6RZJH7WSD2DQDLJ5EDMBSHI6T5IFXLGU4", transactions[0].SourceAccount);
                Assert.AreEqual(66777176560631810, transactions[0].SourceAccountSequence);

                Assert.AreEqual("10/01/2018 3:35:25 AM", transactions[1].CreatedAt.ToString());
                Assert.AreEqual(100, transactions[1].FeePaid);
                Assert.AreEqual("9f6554d4cb0ea467d19d20fe124284e7743d27fa5d54a455ad4237dd045bc0dd", transactions[1].Hash);
                Assert.AreEqual("9f6554d4cb0ea467d19d20fe124284e7743d27fa5d54a455ad4237dd045bc0dd", transactions[1].Id);
                Assert.AreEqual(15549837, transactions[1].Ledger);
                Assert.AreEqual("1041130464", transactions[1].Memo);
                Assert.AreEqual("text", transactions[1].MemoType);
                Assert.AreEqual(1, transactions[1].OperationCount);
                Assert.AreEqual(66786041373134848, transactions[1].PagingToken);
                Assert.AreEqual("jXAtY+kXe79BFpwJcFET+Omhn5yGDZCkMnYqMBZLWbLDQi4ifhli/e0U0O5zXr4MdwTvR+YxCKJiVQpMQdgXBA==", transactions[1].Signatures.Single());
                Assert.AreEqual("GA2VOQDFVRJZZAO7G247YCM6RZJH7WSD2DQDLJ5EDMBSHI6T5IFXLGU4", transactions[1].SourceAccount);
                Assert.AreEqual(66777176560631811, transactions[1].SourceAccountSequence);
                
                Assert.AreEqual("10/01/2018 6:58:15 AM", transactions[2].CreatedAt.ToString());
                Assert.AreEqual(100, transactions[2].FeePaid);
                Assert.AreEqual("c121b1551f2fc5a4beea90fc726474fdcced1359aa9acb46068a5fc37f66f15a", transactions[2].Hash);
                Assert.AreEqual("c121b1551f2fc5a4beea90fc726474fdcced1359aa9acb46068a5fc37f66f15a", transactions[2].Id);
                Assert.AreEqual(15552707, transactions[2].Ledger);
                Assert.AreEqual(null, transactions[2].Memo);
                Assert.AreEqual("none", transactions[2].MemoType);
                Assert.AreEqual(1, transactions[2].OperationCount);
                Assert.AreEqual(66798367929274368, transactions[2].PagingToken);
                Assert.AreEqual("z7pTsLyrQreqC4Vk7AyG+7381K92zq8OwJqCYUhtmf+6/PWAcwwANlpYUUm5BT6XSLlXygexpzA5AmsaL/AHAw==", transactions[2].Signatures.Single());
                Assert.AreEqual("GA2VOQDFVRJZZAO7G247YCM6RZJH7WSD2DQDLJ5EDMBSHI6T5IFXLGU4", transactions[2].SourceAccount);
                Assert.AreEqual(66777176560631812, transactions[2].SourceAccountSequence);
                
                Assert.AreEqual("10/01/2018 7:06:11 AM", transactions[3].CreatedAt.ToString());
                Assert.AreEqual(100, transactions[3].FeePaid);
                Assert.AreEqual("b6ecbad66f2c193e8d3d5e4c78c7d0c26ce5e58da8f8ba47a6d2a012b0ff892b", transactions[3].Hash);
                Assert.AreEqual("b6ecbad66f2c193e8d3d5e4c78c7d0c26ce5e58da8f8ba47a6d2a012b0ff892b", transactions[3].Id);
                Assert.AreEqual(15552803, transactions[3].Ledger);
                Assert.AreEqual("1041130464", transactions[3].Memo);
                Assert.AreEqual("text", transactions[3].MemoType);
                Assert.AreEqual(1, transactions[3].OperationCount);
                Assert.AreEqual(66798780246138880, transactions[3].PagingToken);
                Assert.AreEqual("rKC5nErEFyUOJUdx3UyIgDEyio0CREr1plORLstyFOcU6X248mNl5WFG3dL06WCbQ0Zp7jfjUTN0SUS8HQpGBw==", transactions[3].Signatures.Single());
                Assert.AreEqual("GA2VOQDFVRJZZAO7G247YCM6RZJH7WSD2DQDLJ5EDMBSHI6T5IFXLGU4", transactions[3].SourceAccount);
                Assert.AreEqual(66777176560631815, transactions[3].SourceAccountSequence);
            });
        }

        [Test]
        public async Task PaymentsResponse_IsParsedCorrectly()
        {
            // Arrange
            var expectedUri = $"http://api.test.com/abcdef12345/payments?cursor=20&limit=5";
            var exampleResponse = System.IO.File.ReadAllText("ExampleResponses/Payments.json");

            _httpClientMock.Setup(h => h.GetAsync(expectedUri)).ReturnsAsync(exampleResponse);

            // Act
            var payments = await _client.GetPayments("abcdef12345", 20);

            // Assert
            Assert.AreEqual(5, payments.Count);

            Assert.Multiple(() =>
            {
                Assert.AreEqual(66777176560652289, payments[0].Id);
                Assert.AreEqual(66777176560652289, payments[0].PagingToken);
                Assert.AreEqual("GD6WU64OEP5C4LRBH6NK3MHYIA2ADN6K6II6EXPNVUR3ERBXT4AN4ACD", payments[0].SourceAccount);
                Assert.AreEqual("create_account", payments[0].Type);
                Assert.AreEqual(0, payments[0].TypeIndex);
                Assert.AreEqual("10/01/2018 1:00:25 AM" , payments[0].CreatedAt.ToString());
                Assert.AreEqual("a462fe96e0d6ec15b015634bfc8ec35dddac4af24ae9bd0c76d01d2fbfd27986", payments[0].TransactionHash);

                Assert.AreEqual(66785680595886081, payments[1].Id);
                Assert.AreEqual(66785680595886081, payments[1].PagingToken);
                Assert.AreEqual("GA2VOQDFVRJZZAO7G247YCM6RZJH7WSD2DQDLJ5EDMBSHI6T5IFXLGU4", payments[1].SourceAccount);
                Assert.AreEqual("payment", payments[1].Type);
                Assert.AreEqual(1, payments[1].TypeIndex);
                Assert.AreEqual("10/01/2018 3:28:33 AM", payments[1].CreatedAt.ToString());
                Assert.AreEqual("df4b35184aa40234233b6e5c5d6166439c7641301b4bac987888c096d2e5e36c", payments[1].TransactionHash);
                Assert.AreEqual("native", payments[1].AssetType);
                Assert.AreEqual("GA2VOQDFVRJZZAO7G247YCM6RZJH7WSD2DQDLJ5EDMBSHI6T5IFXLGU4", payments[1].From);
                Assert.AreEqual("GAHK7EEG2WWHVKDNT4CEQFZGKF2LGDSW2IVM4S5DP42RBW3K6BTODB4A", payments[1].To);
                Assert.AreEqual(100.0, payments[1].Amount);

                Assert.AreEqual(66785891049287681, payments[2].Id);
                Assert.AreEqual(66785891049287681, payments[2].PagingToken);
                Assert.AreEqual("GA2VOQDFVRJZZAO7G247YCM6RZJH7WSD2DQDLJ5EDMBSHI6T5IFXLGU4", payments[2].SourceAccount);
                Assert.AreEqual("payment", payments[2].Type);
                Assert.AreEqual(1, payments[2].TypeIndex);
                Assert.AreEqual("10/01/2018 3:32:38 AM", payments[2].CreatedAt.ToString());
                Assert.AreEqual("232d16e3240f96cd110887ed6697f667d232d36b91197ae2fee7ccd1137deec6", payments[2].TransactionHash);
                Assert.AreEqual("native", payments[2].AssetType);
                Assert.AreEqual("GA2VOQDFVRJZZAO7G247YCM6RZJH7WSD2DQDLJ5EDMBSHI6T5IFXLGU4", payments[2].From);
                Assert.AreEqual("GAHK7EEG2WWHVKDNT4CEQFZGKF2LGDSW2IVM4S5DP42RBW3K6BTODB4A", payments[2].To);
                Assert.AreEqual(2900.0, payments[2].Amount);

                Assert.AreEqual(66786041373134849, payments[3].Id);
                Assert.AreEqual(66786041373134849, payments[3].PagingToken);
                Assert.AreEqual("GA2VOQDFVRJZZAO7G247YCM6RZJH7WSD2DQDLJ5EDMBSHI6T5IFXLGU4", payments[3].SourceAccount);
                Assert.AreEqual("payment", payments[3].Type);
                Assert.AreEqual(1, payments[3].TypeIndex);
                Assert.AreEqual("10/01/2018 3:35:25 AM", payments[3].CreatedAt.ToString());
                Assert.AreEqual("9f6554d4cb0ea467d19d20fe124284e7743d27fa5d54a455ad4237dd045bc0dd", payments[3].TransactionHash);
                Assert.AreEqual("native", payments[3].AssetType);
                Assert.AreEqual("GA2VOQDFVRJZZAO7G247YCM6RZJH7WSD2DQDLJ5EDMBSHI6T5IFXLGU4", payments[3].From);
                Assert.AreEqual("GAHK7EEG2WWHVKDNT4CEQFZGKF2LGDSW2IVM4S5DP42RBW3K6BTODB4A", payments[3].To);
                Assert.AreEqual(2980.0, payments[3].Amount);

                Assert.AreEqual(66798780246138881, payments[4].Id);
                Assert.AreEqual(66798780246138881, payments[4].PagingToken);
                Assert.AreEqual("GA2VOQDFVRJZZAO7G247YCM6RZJH7WSD2DQDLJ5EDMBSHI6T5IFXLGU4", payments[4].SourceAccount);
                Assert.AreEqual("payment", payments[4].Type);
                Assert.AreEqual(1, payments[4].TypeIndex);
                Assert.AreEqual("10/01/2018 7:06:11 AM", payments[4].CreatedAt.ToString());
                Assert.AreEqual("b6ecbad66f2c193e8d3d5e4c78c7d0c26ce5e58da8f8ba47a6d2a012b0ff892b", payments[4].TransactionHash);
                Assert.AreEqual("native", payments[4].AssetType);
                Assert.AreEqual("GA2VOQDFVRJZZAO7G247YCM6RZJH7WSD2DQDLJ5EDMBSHI6T5IFXLGU4", payments[4].From);
                Assert.AreEqual("GAHK7EEG2WWHVKDNT4CEQFZGKF2LGDSW2IVM4S5DP42RBW3K6BTODB4A", payments[4].To);
                Assert.AreEqual(1.9999050, payments[4].Amount);
            });
        }
        }
    }
